import { v4 as uuidv4 } from "uuid";

const allAppMixin = {
  data() {
    return {
      title: "",
      description: "",
      selectedNote: null,
    };
  },
  methods: {
    addNote() {
      this.$store.dispatch("addNote", {
        title: this.title,
        description: this.description,
        id: uuidv4(),
        completed: false,
      });
      this.clearFields();
    },
    deleteNote(id) {
      if (window.confirm("delete this note now?")) {
        this.$store.dispatch("deleteNote", id);
      }
    },
    editNote() {
      if (!this.selectedNote) return;
      const editedNote = {
        id: this.selectedNote.id,
        title: this.title,
        description: this.description,
        completed: this.selectedNote.completed,
      };
      this.$store.dispatch("editNote", editedNote);
      this.clearFields();
      this.selectedNote = null;
      this.$refs.modalRef.style.display = "none";
    },
    openModal(note) {
      this.selectedNote = note;
      this.title = note.title;
      this.description = note.description;
      this.$refs.modalRef.style.display = "block";
    },
    showModal(note) {
      this.selectedNote = note;
      this.title = note.title;
      this.description = note.description;
      this.$refs.showModal.style.display = "block";
      this.clearFields();
    },
    hideModal() {
      this.$refs.showModal.style.display = "none";
    },
    clearFields() {
      this.title = "";
      this.description = "";
    },
    closeModalSelf() {
      this.$refs.showModal.style.display = "none";
      this.$refs.modalRef.style.display = "none";
    },
    toggleComplete(id) {
      this.$store.dispatch("toggleComplete", id);
    },
  },
  computed: {
    listOfNotes() {
      return this.$store.state.notesData;
    },
  },
};

export default allAppMixin;
