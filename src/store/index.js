// store.js
import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    notesData: JSON.parse(localStorage.getItem("notes")) || [
      {
        title: "one two three",
        description: "how to describe the one two three",
        id: "1",
        completed: false,
      },
    ],
  },
  actions: {
    addNote: (context, payload) => {
      context.commit("addNote", payload);
    },
    deleteNote: ({ commit }, id) => {
      commit("deleteNote", id);
    },
    editNote: ({ commit }, payload) => {
      commit("editNote", payload);
    },
    toggleComplete: ({ commit }, id) => {
      commit("toggleComplete", id);
    },
  },
  mutations: {
    addNote: (state, payload) => {
      state.notesData.push(payload);
      localStorage.setItem("notes", JSON.stringify(state.notesData));
    },
    deleteNote: (state, id) => {
      state.notesData = state.notesData.filter((note) => note.id !== id);
      localStorage.setItem("notes", JSON.stringify(state.notesData));
    },
    editNote: (state, payload) => {
      state.notesData = state.notesData.map((note) => {
        if (note.id === payload.id) {
          note.title = payload.title;
          note.description = payload.description;
        }
        return note;
      });
      localStorage.setItem("notes", JSON.stringify(state.notesData));
    },
    toggleComplete: (state, id) => {
      state.notesData = state.notesData.map((note) => {
        if (note.id === id) {
          note.completed = !note.completed;
        }
        return note;
      });
      localStorage.setItem("notes", JSON.stringify(state.notesData));
    },
  },
});
